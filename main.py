import predictor.forecast as forecast
from predictor.config import *
from fastapi import FastAPI
from pydantic import BaseModel
from app import swagger
from predictor.train import connection

import psycopg2

app = FastAPI(
    title=f"Diabets Classification API",
    version="0.0.1",
    description=swagger.app_description
)

class DiabetesInput(BaseModel):
    Pregnancies: int
    Glucose: int
    BloodPressure: int
    SkinThickness: int
    Insulin: int
    BMI: float
    DiabetesPedigreeFunction: float
    Age: int


@app.post("/fc")
def predict_diabetes(data: DiabetesInput):
    res = forecast.forecast(data)
    response = bool(res[1])
    return {"is diabet" : response}

@app.post("/train")
def train():
    connection()
