from dotenv import load_dotenv
import os

LIMITS = {
    "Pregnancies": {
        "min": -9.0,
        "max": 16.0
    },
    "Glucose": {
        "min": 16.5,
        "max": 222.75
    },
    "BloodPressure": {
        "min": 26.0,
        "max": 116.0
    },
    "SkinThickness": {
        "min": -64.0,
        "max": 96.0
    },
    "Insulin": {
        "min": -254.5,
        "max": 381.75
    },
    "BMI": {
        "min": 8.7,
        "max": 55.2
    },
    "DiabetesPedigreeFunction": {
        "min": -0.5212499999999999,
        "max": 1.3912499999999999
    },
    "Age": {
        "min": -10.0,
        "max": 75.0
    }
}

R_SCALER_PATH = "./model/Rscaler.joblib"
MODEL_PATH = "./model/model.joblib"

load_dotenv()
DB_HOST = os.getenv('DB_HOST')
DB_PORT =os.getenv('DB_PORT')
DB_NAME= os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')