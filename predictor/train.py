import psycopg2
import pandas as pd

from predictor.config import DB_NAME, DB_USER, DB_PASSWORD, DB_HOST, DB_PORT

def connection():
    # Подключение к базе данных PostgreSQL
    conn = psycopg2.connect(
        dbname=DB_NAME,
        user=DB_USER,
        password=DB_PASSWORD,
        host=DB_HOST,
        port=DB_PORT
    )

    # Загрузка существующей структуры таблицы из базы данных
    existing_data = pd.read_sql_query("SELECT * FROM newtable LIMIT 0", conn)

    # Загрузка новых данных из CSV файла
    new_data = pd.read_csv("./data/diabetes.csv")

    # Сравнение структуры таблиц и проверка наличия одинаковых записей
    existing_columns = set(existing_data.columns)
    new_columns = set(new_data.columns)

    if existing_columns != new_columns:
        conn.close()
        raise ValueError("Структура таблицы не совпадает с данными из файла CSV.")

    existing_values = existing_data.to_dict(orient="records")
    new_values = new_data.to_dict(orient="records")

    for new_record in new_values:
        if new_record in existing_values:
            print(f"Запись {new_record} уже существует.")

    # Добавление новых данных в таблицу
    new_data.to_sql("newtable", conn, if_exists="append", index=False)

    # Закрытие соединения с базой данных
    conn.close()
