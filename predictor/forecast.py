import pandas as pd
from joblib import load

import predictor.config as config

r_scaler = load(config.R_SCALER_PATH)
model = load(config.MODEL_PATH)


def forecast(data: dict) -> int:
    for key in data:
        if not config.LIMITS[key]['min'] <= data[key] <= config.LIMITS[key]['max']:
            raise ValueError("Your data is out of range!")

    df = pd.DataFrame(columns=data.keys())
    for column, limits in data.items():
        df[column] = [limits]
    
    s_df = r_scaler.transform(df)
    result = model.predict(s_df)
    return result
